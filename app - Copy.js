const { Worker } = require('worker_threads');
const { WebSocket } = require("ws");
const numCPUs = require('os').cpus().length;
const numWorker = 1;

// variable
const workers = [];
const mainStatus = {
    job: null,
    totalHashed: 0,
    ws: null,
    attempts: 0,
    servers: ["wss://node-proxy-1.up.railway.app/"]
};

setInterval(() => {
    console.log('[Speed]: ', Math.floor(mainStatus.totalHashed / 60));
    ref(mainStatus, 'totalHashed', 0);
}, 60000);


/* --------------------------Help Fn------------------------  */
const ref = (orign, key, update) => orign[key] = update;


/* --------------------------Worker--------------------------  */
const fn_firstCreateWorker = () => {
    for (let idWorker = 0; idWorker / 1000 < (numWorker || numCPUs); idWorker += 1000) {

        workers.push(new Worker(`./worker${idWorker / 1000 || ''}.js`));
        workers[workers.length - 1].on('message', message => fn_receiveMessageWorker(message));
        workers[workers.length - 1].on('error', error => fn_receiveErrorWorker(error));
        workers[workers.length - 1].on('exit', code => fn_exitErrorWorker(code));

    }
}
fn_firstCreateWorker();

const fn_receiveMessageWorker = (message) => {
    try {
        switch (message.type) {
            case 'solved':
                ws.send(JSON.stringify(message.data));
                fn_sendJobToWorker(mainStatus.job);
                ref(mainStatus, 'totalHashed', mainStatus.totalHashed + 1);
                console.log(message.data);
                break;
            case 'hashed':
                fn_sendJobToWorker(mainStatus.job);
                ref(mainStatus, 'totalHashed', mainStatus.totalHashed + 1);
                break;
            default:
                break;
        }
    } catch (error) {
        console.log(error);
    }
}
const fn_receiveErrorWorker = (error) => {
    console.log(error)
}
const fn_exitErrorWorker = (code) => {
    if (code !== 0)
        console.log(`stopped with  ${code} exit code`);
}
const fn_sendJobToWorker = (message) => {
    workers.forEach(worker => worker.postMessage({
        type: 'job',
        data: message
    }))
}



/* --------------------------Main---------------------------- */
const fn_receiveMessageServer = (message) => {
    try {
        message = JSON.parse(message.toString('utf8'));
        if (message.identifier) {

            switch (message.identifier) {
                case "job":
                    // console.log(message);
                    message["headerBlob"] = message.blob.substring(0, 78);
                    message["footerBlob"] = message.blob.substring(86, message.blob.length);
                    delete message["blob"];
                    delete message["identifier"];
                    ref(mainStatus, 'job', message);
                    fn_sendJobToWorker(message);
                    console.log('[Server]:', message.job_id + '##' + message.height);
                    // console.log(mainStatus);
                    break;
                case "hashsolved":
                    console.log('[Server]: hashsolved!');
                    break;
                default:
                    console.log(message);
                    break;
            }

        }
    } catch (error) {
        console.log(error);
    }
}


const fn_heartbeat = () => {
    mainStatus.ws.pong();
    clearTimeout(mainStatus.ws.pingTimeout);
    mainStatus.ws.pingTimeout = setTimeout(() => {
        mainStatus.ws.terminate();
    }, 60000 + 5000);
}
const connectSocket = () => new Promise((resolve, reject) => {

    console.log('[Main]: connecting to server!')
    ref(mainStatus, 'attempts', mainStatus.attempts + 1);
    if (mainStatus.ws != null) {
        mainStatus.ws.close();
    }
    const server = mainStatus.servers[Math.floor(Math.random() * mainStatus.servers.length)];
    ref(mainStatus, 'ws', new WebSocket(server));
    mainStatus.ws.on('message', message => fn_receiveMessageServer(message));
    mainStatus.ws.on('open', () => {
        fn_heartbeat();
        console.log('[Main]: connected to server!');
        // mainStatus.ws.send((JSON.stringify(mainStatus.handshake)));
        ref(mainStatus, 'attempts', 0);
    });
    mainStatus.ws.on('error', () => {
        console.log('[Server]: erorr!');
        clearTimeout(mainStatus.ws.pingTimeout);
        return reject();
        // job = null;
    });
    mainStatus.ws.on('close', () => {
        console.log('[Server]: disconnected!');
        clearTimeout(mainStatus.ws.pingTimeout);
        return reject();
        // job = null;
    });
    mainStatus.ws.on('ping', () => fn_heartbeat())
}).catch(async () => {
    ref(mainStatus, 'ws', null);
    console.log('[Main]: The WebSocket is not connected. Trying to connect after', mainStatus.attempts * 10, 's');
    await new Promise(resolve => setTimeout(resolve, 10000 * mainStatus.attempts));
    connectSocket();
});
connectSocket();